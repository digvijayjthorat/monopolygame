package gameboard;

public interface Property 
{
		void affect(Player p);
		Player getOwner();
		String getSetName();
		String getName();
		String getText();
		int getHouses();
		int getCost();
		void setOwner(Player p);
		boolean isMonopoly();
		void addHouse();
		int getRent();
		boolean hotelCheck();
		boolean canBuild();
		boolean canSell();
		void sellHouse();
		int getHouseCost();
		void info();
}
