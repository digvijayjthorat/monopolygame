package gameboard;

public abstract class Board {
	private int pos; // 0~39, indicating the tile number on the gameboard.
	
	public Board (int pos){
		this.pos = pos;
	}
	
	/* (non-Javadoc)
	 * @see gameboard.Square#Action(player.Player)
	 */
	public abstract void Action(Player player);
	
	/* (non-Javadoc)
	 * @see gameboard.Square#getPos()
	 */
	public int getPos() {
		return pos;
	}
}
