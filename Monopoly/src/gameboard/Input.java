package gameboard;

import java.util.Scanner;
public class Input {
	private final Scanner scanner;

	public Input() {
		scanner = new Scanner(System.in);
	}

	public String inputString() {
		return scanner.nextLine();
	}

	public void pressEnter()
	{
	 scanner.nextLine();
	}
	public boolean inputBool() {
		return inputDecision(new String[]{"Yes", "No"}) == 0;
	}

	public int inputInt() {
		while (true) {
			int val;
			try {
				val = Integer.parseInt(inputString());
			} catch (NumberFormatException e) {
				System.out.println("Please enter a valid integer.");
				continue;
			}
			return val;
		}
	}

	public int inputDecision(String[] choices) {
		while (true) {
			String input = inputString();
			for (int i = 0; i < choices.length; i++) {
				if (input.equalsIgnoreCase(choices[i]) || input.equalsIgnoreCase(choices[i].substring(0, 1)))
					return i;
			}
			System.out.println("Please enter a valid decision.");
		}
	}

	public Player inputPlayer(Iterable<Player> players, Player notAllowed) {
		Player player = null;
		do {
			String name = inputString();
			for (Player p : players) {
				if (name.equals(p.getName()))
					player = p;
			}
			if (player == null)
				System.out.println("Invalid player, please enter another name.");

			else if (notAllowed != null && player.getName().equals(notAllowed.getName())) {
				System.out.println("You may not select this player. Choose another.");
				player = null;
			}
		} while (player == null);

		return player;
	}
}