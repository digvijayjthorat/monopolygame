package gameboard;

public interface InputDice {

	//Set random number for the two dices.
	void roll();

	//Manually set dices. For test purpose only.
	void roll(int num1, int num2);

	boolean isDouble();

	int doubleTimes();

	int getResult();

}