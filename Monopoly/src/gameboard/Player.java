package gameboard;
import java.util.ArrayList;

public class Player {
	private String name;
	private int money;
	private int location; //0~39, indicating player's current tile.
	private int inJail; //= 1: in jail; = 0: not in jail.
	private int attempt;// Times of attempt to get out of jail.
	private int getOutOfJailFreeCount;
	private int totalPlayers;
	private ArrayList<Property> myProps;
	ArrayList<String> myProps1;
	public ArrayList<String> getMyProps1() {
		return myProps1;
	}

	public void setMyProps1(ArrayList<String> myProps1) {
		this.myProps1 = myProps1;
	}

	
	public Player(String name) {
		this.name = name;
		this.money = 1500;
		this.inJail = 0;
		this.location = 0;
		this.totalPlayers= 0;
		this.getOutOfJailFreeCount = 0;
		myProps=new ArrayList<Property>();
	}
	
	public void setLocation(int location) {
		if(location > 39) {
			location -= 40;
			System.out.println(this.name + " gets 200 dollars for salary.");
			this.money += 200;
		}
		this.location = location;
		System.out.println(this.name + " is on block " + this.location);
	}
	
	public int getLocation() {
		return this.location;
	}
	
	public void setMoney(int money) {
		this.money += money;
		System.out.println(this.name + " now has " + this.money + " dollars.");
	}
	
	public int getMoney() {
		return this.money;
	}
	
	public String getName() {
		return this.name;
	}
	
	public void goToJail() {
		this.inJail = 1;
		this.attempt = 0;
		setLocation(10);
		System.out.println(this.name + " is in jail.");
	}
	
	public void outOfJail() {
		this.inJail = 0;
		this.attempt = 0;
		System.out.println(this.name + " is out of jail.");
	}
	
	public boolean isInJail() {
		if(this.inJail == 0) return false;
		else return true;
	}
	
	
	public void increaseAttempt() {
		this.attempt ++;
	}
	
	public int getAttempt() {
		return this.attempt;
	}
	
	public void useJailCard() {
		this.getOutOfJailFreeCount -= 1;//use a get out of jail free card
	    }
	
	 public void addJailCard() {
			this.getOutOfJailFreeCount += 1;//add a get out of jail free card
		    }
	 
	 public boolean haveJailCard() {
		 if(getOutOfJailFreeCount >0) return true;
		 else return false;
	 }
	 
	 public void setTotalPlayer(int totalPlayers )
	 {
		 this.totalPlayers += totalPlayers;
	 }
	 
	 public int getTotalPlayers()
	 {
		 return this.totalPlayers;
	 }

	public ArrayList<Property> getMyProps() {
		return myProps;
	}
	public void pay(int amount)
	{
		money-=amount;
	}
	public void pay(Player p, int amount) 
	{
		pay(amount);
		p.earn(amount);
		
	}
	public void earn(int amount)
	{
		money+=amount;
	}
	public void get(Property p){
		myProps.add(p);
	}
	
	//This function is suppose to calculate the total value of this player.
	public int checkValue() {
		int value =0;
		if(!(myProps == null))
			for(Property p : myProps)
				value += p.getCost()+p.getHouses()*p.getHouseCost();
		return value;
	}
	
	public boolean bankruptcy() {
		int value = 0;
		if(!(myProps ==null))
		for(Property p : myProps)
			value += p.getCost()+0.5*p.getHouses()*p.getHouseCost();
		return (money < value);
	}

}
