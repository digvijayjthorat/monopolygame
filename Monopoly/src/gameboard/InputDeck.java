package gameboard;

public interface InputDeck {

	void shuffle();

	String print();

	void next(Player player);

}