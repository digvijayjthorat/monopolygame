package gameboard;


import java.util.Random;

public class Dices implements InputDice {
	private int dice1;
	private int dice2;
	private int doubleTimes;
	private boolean isDouble;
	private Input input;
	private Backdoor backdoor;
	
	
	public Dices() {
		doubleTimes = 0;
		isDouble = false;
		dice1 = 0;
		dice2 = 0;
		input= new Input();
	}
	
	//Set random number for the two dices.
	/* (non-Javadoc)
	 * @see gameboard.InputDice#roll()
	 */
	@Override
	public void roll() {
		Random r1 = new Random();
		Random r2 = new Random();
		backdoor = new Backdoor();
		
		System.out.print("Press Enter to roll the dices! ");
		if(backdoor.check(input.inputString())) {
			System.out.println("Mannually set dice enabled.");
			System.out.print("Set Dice1: ");
			dice1 = input.inputInt();
			System.out.print("Set Dice2: ");
			dice2 = input.inputInt();
		}
		else {
		dice1 = r1.nextInt(6)+1;
		dice2 = r2.nextInt(6)+1;
		}
		System.out.println("Dice1: " + dice1 + "; Dice2: " + dice2);
		
		if(dice1 == dice2) {
			System.out.println(" Doubles!");
			isDouble = true;
			doubleTimes++;
		}
		else isDouble = false;
	}
	
	//Manually set dices. For test purpose only.
	/* (non-Javadoc)
	 * @see gameboard.InputDice#roll(int, int)
	 */
	@Override
	public void roll(int num1, int num2) {
		
		System.out.print("Press Enter to roll the dices! ");
		input.pressEnter();
		
		dice1 = num1;
		dice2 = num2;
		System.out.println("Manually set dices. Dice1: " + dice1 + "; Dice2: " + dice2);
		
		if(dice1 == dice2) {
			System.out.println(" Doubles!");
			isDouble = true;
			doubleTimes++;
		}
	}
	
	/* (non-Javadoc)
	 * @see gameboard.InputDice#isDouble()
	 */
	@Override
	public boolean isDouble() {
		return isDouble;
	}
	
	/* (non-Javadoc)
	 * @see gameboard.InputDice#doubleTimes()
	 */
	@Override
	public int doubleTimes() {
		return doubleTimes;
	}
	
	/* (non-Javadoc)
	 * @see gameboard.InputDice#getResult()
	 */
	@Override
	public int getResult() {
		return dice1 + dice2;
	}
}
