package gameboard;

public class LuxuryTax extends Square{

	public LuxuryTax(int pos) {
		super(pos);
	}
	
	public void Action(Player player) {
		System.out.println("Luxury Tax. "+player.getName()+" looses 100 dollars");
		player.setMoney(-100);
	}
}
