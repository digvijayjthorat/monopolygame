package gameboard;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

class Game {
	private List<Square> squares;
	private List<Player> players;
	private Player player= new Player("");
	private Input input;
	private Backdoor backdoor;
	private Bank bank;
	private InputDice dice;
	private int playerRemoved;
	private Player winner;
	private Game()
	{
		players = new ArrayList<>();
		input = new Input();
		playerRemoved = 0;
		bank = new Bank();
		System.out.println("Quick initialization? (Yes/No)");
		if (input.inputBool()) quickInitialization(players);
		else initializePlayers(input);
		bank.updatePlayers(players);
		initializeGameboard();
	}
	
	//Setup players. Configure their names and turn order.
	private void initializePlayers(Input input) {
		System.out.println("Initializing players...");
		System.out.println("How many total players?");
		
		int N = input.inputInt();
		player.setTotalPlayer(N);
		while (N < 2 || N > 6) {
			
			System.out.println("Must have between 2 and 6 players. Please try again.");
			if (N < 1)
				System.out.println("Must have at least one human player. Please try again.");
			if (N > 6)
				System.out.println("Cannot have more human players than total players. Please try again.");
			N = input.inputInt();
			
		}		
		
		int[] order = new int[N];
		for (int i = 0; i < N; i++) {
			System.out.println("Player " + (i + 1) + " name?");
            players.add(new Player(input.inputString()));
		}
		
		System.out.println("done.");
	}
	
	//Setup game board. Configure the type of squares and their position on the game board.
	private void initializeGameboard() {
		
		System.out.print("Initializing gameboard...");
		
		squares = new ArrayList<>(40);
		
		for(int i = 0; i < 40; i++)  {
			Square square = new EmptySquare (i);
			squares.add(i,square);
		}
		
		Square square0 = new Go(0);
		Square square4 = new IncomeTax(4);
		Square square5 = new Railroad(5);
		Square square10 = new Jail(10);
		Square square15 = new Railroad(15);
		Square square20 = new FreeParking(20);
		Square square25 = new Railroad(25);
		Square square30 = new GotoJail(30);
		Square square35 = new Railroad(35);
		Square square38 = new LuxuryTax(38);
		
		squares.add(0,square0);
		squares.add(4,square4);
		squares.add(5,square5);
		squares.add(10,square10);
		squares.add(15,square15);
		squares.add(20,square20);
		squares.add(25,square25);
		squares.add(30,square30);
		squares.add(35,square35);
		squares.add(38,square38);
		
		
		System.out.println("Done.");
	}
	
	//Decide what the player should do on this square, based on the player's location.
	public void handleSquare(Player player) { 
		
		Square block = squares.get(player.getLocation());
		try {
			block.Action(player);
		} catch (Exception e) {
			bank.auction(player, squares.get(player.getLocation()).getProterty(player.getLocation()));
		}
		
	}
	
	private void startTurn(int turn, int index) {
		Player currentPlayer;
		//Decide the current player, based on the number of turn. It will cycle through all players.
		if(index == players.size()) index = index%players.size();
		currentPlayer = players.get(index); 
		System.out.println("Turn " + turn + ". It is " + currentPlayer.getName() + "'s turn.");
		printState(currentPlayer);
		System.out.println("Press Enter to continue.");
		
		dice = new Dices();
		backdoor = new Backdoor();
		if(backdoor.check(input.inputString())) backdoor.handle(input,currentPlayer);
		if(backdoor.skipTurn()) startTurn(turn + 1, index+1);
		else {
		// First check if the current player is in jail.
		if(currentPlayer.isInJail()) handleSquare(currentPlayer); 
		
		//If the current player is not in jail, the player can proceed the turn normally.
		if(!currentPlayer.isInJail()) {
			dice.roll();
			currentPlayer.setLocation(currentPlayer.getLocation() + dice.getResult()); //Move the token
			handleSquare(currentPlayer);
			
			//If the player roll double less than 3 times in a roll, the player can roll again.
			while (dice.isDouble() && dice.doubleTimes() < 3 && !currentPlayer.isInJail()) {
				dice.roll( );
				currentPlayer.setLocation(currentPlayer.getLocation() + dice.getResult()); //Move the token
				handleSquare(currentPlayer);
			}
			
			if (dice.doubleTimes() == 3) {
				System.out.println("Three doubles in a roll. " + currentPlayer.getName() + " is sent to jail. Press Enter to continue.");
				input.pressEnter();
				currentPlayer.goToJail();
			}
		}
		
		//Bankruptcy check.
		if(currentPlayer.bankruptcy()) {
			System.out.println(currentPlayer.getName() + " is bankrupt. Remove from game.");
			players.remove(players.indexOf(currentPlayer));
			playerRemoved++;
			if(players.size() ==1) {
				System.out.println("Game Over. Winner is "+players.get(0).getName()); // if only 2 players, one goes bankruptcy the other one wins.
				main(null);
			}
			if(index == players.size()+1) index = players.size();
			for(Property p : currentPlayer.getMyProps()) {
				p.setOwner(null);
				bank.auction(players.get(index), p);
			}
			
			if(playerRemoved ==1) startTurn(turn+1, index);
		}
		
		//Endgame check.
		if(playerRemoved >1) {
			winner = players.get(0);
			for(int i = 0; i<players.size();i++) 
				if ((players.get(i)).checkValue() > winner.checkValue()) winner = players.get(i);
			System.out.println("Game Over. Winner is " + winner.getName());
			main(null);
		}
		
		//Additional actions
		additionalActions(currentPlayer,turn,index);
		
		}
	}
	
	private void printState(Player player) {
		    System.out.println("--------------------------------------------------");
			System.out.printf("%-10s%40s%n", "Name : ", player.getName());
			System.out.printf("%-10s%40s%n", "Money: ", player.getMoney());
			System.out.printf("%-10s%40s%n", "Position ", player.getLocation());
			System.out.printf("Properties: ");	//	Iterable<Square> owned = player.properties();
			for(Property p : player.getMyProps())
				System.out.println(p.getName()+"("+p.getHouses()+")"+"("+p.getSetName()+")");
			System.out.println("--------------------------------------------------");
		}
	
	//Initialize N players with naming Player1, Player2...
	private void quickInitialization(List<Player> players) {
		System.out.println("How many total players?");
		int N = input.inputInt();
		while (N < 2 || N > 6) {
			System.out.println("Must have between 2 and 6 players. Please try again.");
			if (N < 1)
				System.out.println("Must have at least one human player. Please try again.");
			if (N > 6)
				System.out.println("Cannot have more human players than total players. Please try again.");
			N = input.inputInt();
		}	
		for (int i=1;i<=N;i++)
			players.add(new Player("Player" + i+""));
	}
	
	private void additionalActions(Player player, int turn, int index) {
		System.out.println("Additional actions for "+player.getName()+"'s turn");
		System.out.println("1. Next turn.");
		System.out.println("2. Show player info.");
		System.out.println("3. Buy house.");
		System.out.println("4. Buy hotel.");
		System.out.println("5. Sell house to the bank.");
		System.out.println("6. Sell hotel to the bank.");
		System.out.println("7. Sell property to another player.");
		
		int N = input.inputInt();
		switch(N) {
		case 1: startTurn(turn+1,index+1);break;
		case 2: handleShowInfo(players,player,turn,index);break;
		case 3: handleBuyHouse(player,turn,index);break;
		case 4: handleBuyHotel(player,turn,index);break;
		case 5: handleSellHouse(player,turn,index);break;
		case 6: handleSellHotel(player,turn,index);break;
		case 7: handleSellProperty(players,player,turn,index);break;
		
		default: System.out.println("Invalid input. Please choose the number accordingly.");additionalActions(player,turn,index);
		}
	}
	
	private void handleBuyHouse(Player player, int turn,int index) {
		if(bank.house<=0) {
			System.out.println("Building Shortages. There is no house in the bank for sale. Press Enter to return to previous menu.");
			input.pressEnter();
			additionalActions(player,turn,index);
		} else {
			int i=0;
			List<Property> properties = new ArrayList<>();
			System.out.println("Where do you want to erect the house on?");
			
			for (Property p: player.getMyProps()) 
				if (p.isMonopoly() && p.canBuild() && p.getHouses()<4) {
					i++;
					System.out.println(i+". "+p.getName());
					properties.add(p);
				}
			
			if (i==0) {
				System.out.println("No property is suitable for building. Press Enter to return to previous menu.");
				input.pressEnter();
				additionalActions(player,turn,index);
			}else {
			System.out.println((i+1)+". Back.");
			int N = input.inputInt() ;
			if(N == properties.size()+1) additionalActions(player,turn,index);
			else if(N>properties.size()+1) {
				System.out.println("Invalid number. Please choose the number accordingly.");
				handleBuyHouse(player,turn,index);
			}else {
				properties.get(N-1).addHouse();
				bank.house--;
				System.out.println(properties.get(N-1).getName()+" now has "+properties.get(N-1).getHouses()+" house(s).");
				System.out.println("Number of houses in the bank: "+bank.house);
				additionalActions(player,turn,index);
			}
			}
		}
	}
	
	private void handleBuyHotel(Player player, int turn, int index) {
		if(bank.hotel<=0) {
			System.out.println("Building Shortages. There is no hotel in the bank for sale. Press Enter to return to previous menu.");
			input.pressEnter();
			additionalActions(player,turn,index);
		} else {
			int i=0;
			List<Property> properties = new ArrayList<>();
			System.out.println("Where do you want to erect the hotel on?");
			
			for (Property p: player.getMyProps()) 
				if (p.isMonopoly() && p.hotelCheck() && p.getHouses()==4) {
					i++;
					System.out.println(i+". "+p.getName());
					properties.add(p);
				}
			
			if (i==0) {
				System.out.println("No property is suitable for building. Press Enter to return to previous menu.");
				input.pressEnter();
				additionalActions(player,turn,index);
			}else {
			System.out.println((i+1)+". Back.");
			int N = input.inputInt();
			if(N == properties.size()+1) additionalActions(player,turn,index);
			else if(N>properties.size()+1) {
				System.out.println("Invalid number. Please choose the number accordingly.");
				handleBuyHotel(player,turn,index);
			}else {
				properties.get(N-1).addHouse();
				bank.hotel--;
				bank.house+=4;
				System.out.println(properties.get(N-1).getName()+" now has a hotel.");
				System.out.println("Number of hotel in the bank: "+bank.hotel);
				System.out.println("Number of house in the bank: "+bank.house);
				additionalActions(player,turn,index);
			}
			}
		}
	}
	
	private void handleSellHouse(Player player, int turn, int index) {
		int i = 0;
		List<Property> properties = new ArrayList<>();
		System.out.println("From which property do you want to sell house?");
		
		for (Property p: player.getMyProps()) 
			if (p.canSell() && p.getHouses() > 0 && p.getHouses() <5) {
				i++;
				System.out.println(i+". "+p.getName());
				properties.add(p);
			}
		
		if (i==0) {
			System.out.println("No house is suitable for selling. Press Enter to return to previous menu.");
			input.pressEnter();
			additionalActions(player,turn,index);
		}else {
		System.out.println((i+1)+". Back.");
		int N = input.inputInt() ;
		if(N == properties.size()+1) additionalActions(player,turn,index);
		else if(N>properties.size()+1) {
			System.out.println("Invalid number. Please choose the number accordingly.");
			handleSellHouse(player,turn,index);
		}else {
			properties.get(N-1).sellHouse();
			bank.house++;
			System.out.println(properties.get(N-1).getName()+" now has "+properties.get(N-1).getHouses()+" house(s).");
			System.out.println("Number of houses in the bank: "+bank.house);
			additionalActions(player,turn,index);
		}
		}
	
	}
	
	private void handleSellHotel(Player player, int turn, int index) {
		if(bank.house<4) {
			System.out.println("There aren't enough houses in the bank. Press Enter to return to previous menu.");
			System.out.println("Number of house in the bank: "+bank.house);
			input.pressEnter();
			additionalActions(player,turn,index);
		}else {
		int i = 0;
		List<Property> properties = new ArrayList<>();
		System.out.println("From which property do you want to sell hotel?");
		
		for (Property p: player.getMyProps()) 
			if (p.canSell() && p.getHouses() == 5) {
				i++;
				System.out.println(i+". "+p.getName());
				properties.add(p);
			}
		
		if (i==0) {
			System.out.println("No hotel is suitable for selling. Press Enter to return to previous menu.");
			input.pressEnter();
			additionalActions(player,turn,index);
		}else {
		System.out.println((i+1)+". Back.");
		int N = input.inputInt() ;
		if(N == properties.size()+1) additionalActions(player,turn,index);
		else if(N>properties.size()+1) {
			System.out.println("Invalid number. Please choose the number accordingly.");
			handleSellHotel(player,turn,index);
		}else {
			properties.get(N-1).sellHouse();
			bank.hotel++;
			bank.house-=4;
			System.out.println(properties.get(N-1).getName()+" now has "+properties.get(N-1).getHouses()+" house(s).");
			System.out.println("Number of hotel in the bank: "+bank.hotel);
			System.out.println("Number of house in the bank: "+bank.house);
			additionalActions(player,turn,index);
		}
		}
		}
	}
	
	private void handleShowInfo(List<Player> players,Player player,int turn,int index) {
		System.out.println("Hello "+player.getName()+". Whose info do you want to see?");
		for(int i=0;i<players.size();i++) 
			System.out.println((i+1)+". "+players.get(i).getName());
		int N = input.inputInt();
		if(N<=players.size())
		printState(players.get(N-1));
		else System.out.println("Invalid input.");
		additionalActions(player,turn,index);
		
	}
	
	private void handleSellProperty(List<Player> players,Player player,int turn,int index) {
		System.out.println("Which property do you want to sell?");
		List<Property> properties = new ArrayList<>();
		int i=0;
		
		for (Property p: player.getMyProps()) 
			if (p.canSell() && p.getHouses() ==0) {
				i++;
				System.out.println(i+". "+p.getName());
				properties.add(p);
			}
		
		if (i==0) {
			System.out.println("No property is suitable for selling. Press Enter to return to previous menu.");
			input.pressEnter();
			additionalActions(player,turn,index);
		}else {
			System.out.println((i+1)+". Back.");
			int N1 = input.inputInt() ;
			if(N1 == properties.size()+1) additionalActions(player,turn,index);
			else if(N1>properties.size()+1) {
				System.out.println("Invalid number. Please choose the number accordingly.");
				handleSellProperty(players,player,turn,index);
			}else {
			System.out.println("What's the price?");
			int price = input.inputInt();
			System.out.println("To which player do you want to sell your property?");
			for(int j=0;j<players.size();j++) 
				if(!(players.get(j).getName().equalsIgnoreCase(player.getName())))
					System.out.println((j+1)+". "+players.get(j).getName());
			int N2 = input.inputInt();
			if(N2>players.size()) {
				System.out.println("Invalid input.");
				additionalActions(player,turn,index);
			}else {
				System.out.println(players.get(N2-1).getName()+". Do you want to buy "+properties.get(N1-1).getName()+" for "+price+"dollars?");
				if(input.inputBool()) {
					players.get(N2-1).pay(player, price);
					player.getMyProps().remove(properties.get(N1-1));
					properties.get(N1-1).setOwner(players.get(N2-1));
					players.get(N2-1).get(properties.get(N1-1));
				}
				additionalActions(player,turn,index);
			}
			}
		}
	}
	
public static void main(String[] args) {
		Game monopolyGame= new Game();
		monopolyGame.startTurn(1,0);
	}

}
