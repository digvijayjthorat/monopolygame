package gameboard;

public class IncomeTax extends Square{
	
	public IncomeTax(int pos) {
		super(pos);
	}
	
	public void Action(Player player) {
		System.out.println("Income Tax. "+player.getName()+" looses 200 dollars");
		player.setMoney(-200);
	}

}
