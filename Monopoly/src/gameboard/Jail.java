package gameboard;
import java.util.Scanner;

public class Jail extends Square{
	private Input input;

	public Jail(int pos) {
		super(pos);
		input= new Input();
	}

	@Override
	public void Action(Player player) {
		
		//First check if the player is in jail, and if the player has exceeded the number of attempts. 
		if(player.isInJail() & player.getAttempt() < 2) {
			System.out.println(player.getName() + " is in jail. Choose actions.");
			System.out.println("1. Pay the fine.");
			System.out.println("2. Roll doubles to get out.");
			System.out.println("3. Use Get Out of Jail card.");
			
			//Still need to handle illegal input. (If the user input is not a integer.)
			switch (input.inputInt()){
			case 1:{
				player.setMoney(-50);
				player.outOfJail();
				break;
			}
			case 2:{
				System.out.println("Attempt " + (player.getAttempt() + 1) + " to get out of jail.");
				InputDice dices = new Dices();
				dices.roll();
				if(dices.isDouble()) {
					System.out.print("Success!");
					player.outOfJail();
					player.setLocation(10 + dices.getResult());
					//Game.handleSquare(player);
				}
				else {
					System.out.println("Fail. "+ player.getName() + " is still in jail");
					player.increaseAttempt();
				}
				break;
			}
			case 3:{
				if(player.haveJailCard()) {
					player.useJailCard();
					player.outOfJail();
					break;
				}
				else {
					System.out.println(player.getName() + " doesn't have Get Out of Jail card.");
					Action(player);
					break;
				}
			}
			default :{
				System.out.println("Invalid input. Press the number accordingly.");
				Action(player);
				break;
			}
			}
		}
		
		//If the player has exceeded the number of attempts, then the player must pay the fine.
		else if(player.getAttempt() == 2) {
			System.out.print("Three turns in jail. " + player.getName() + " must pay the 50-dollar fine. Press Enter.");
			 input.pressEnter();
			
			player.setMoney(player.getMoney() - 50);
			player.outOfJail();
			
		}
		
		//If the player's status is not in jail, then the player is just passing by.
		else System.out.println(player.getName() + " passes through jail.");
	}
	
	
}
