package gameboard;

public class PlayerCard extends Card {
    private String text;
    private int amount;
    
    public PlayerCard (int value, String text){
	this.text = text;
	this.amount = value;
    }
    
    public void execute (Player p){
	int temp = p.getTotalPlayers();
	p.setMoney(-amount*temp);
	for (int i = 0; i<temp;i++){
	    p.setMoney(amount);
	}
    }
    public String printText(){
	return this.text;
    }
}