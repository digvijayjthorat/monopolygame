package gameboard;

public class Street implements Property 
{
	private String name; //name of the property
	private String setName; //color of the  property
	private int cost; //cost to buy the property
	private int setNum; //how many properties are in this set
	private int rent; //rent of the property
	private int rent0House; // rent when there is no house
	private int rent1House; //rent when there is one house
	private int rent2House; //rent when there is two houses
	private int rent3House; //rent when there is three houses
	private int rent4House; //rent when there is four houses
	private int rentHotel; //rent of the hotel
	private int houses; //number of the houses
	private int houseCost; //cost to buy a house
	private Player owner;
	
	


	public Street(String name, String setName, int setNum, int rent0House, int rent1House, int rent2House,
			int rent3House, int rent4House, int rentHotel, int houseCost, int cost) {
		super();
		this.name = name;
		this.setName = setName;
		this.setNum = setNum;
		this.rent0House = rent0House;
		this.rent1House = rent1House;
		this.rent2House = rent2House;
		this.rent3House = rent3House;
		this.rent4House = rent4House;
		this.rentHotel = rentHotel;
		houses=0;
		this.rent = rent0House;
		this.houseCost = houseCost;
		owner= null;
		this.cost=cost;
	}

	//getter of name
	public String getName() 
	{
		return name;
	}
	

	//getter of cost
	public int getCost() {
		return cost;
	}
	
	//getter of setNum
	public int getSetNum() {
		return setNum;
	}

	//getter of rent
	public int getRent() {
		return rent;
	}

	//getter of rent1House
	public int getRent1House() {
		return rent1House;
	}


	//getter of rent2House
	public int getRent2House() {
		return rent2House;
	}

	//getter of rent3House
	public int getRent3House() {
		return rent3House;
	}

	//getter of rent4House
	public int getRent4House() {
		return rent4House;
	}

	//getter of rentHouse
	public int getRentHotel() {
		return rentHotel;
	}

	//getter of Houses
	public int getHouses() {
		return houses;
	}

	//getter of houseCost
	public int getHouseCost() {
		return houseCost;
	}
	
	//assigning the rent of the house
	public void addHouse()
	{
		if(houses <5)
		{
			if (houses == 0)
			rent = rent1House;
			if (houses == 1)
			rent = rent2House;
			if (houses == 2)
			rent = rent3House;
			if (houses == 3) 
			rent = rent4House;
			if (houses == 4)
			rent = rentHotel;
			houses++;
			owner.pay(houseCost);
		}
	}
	
	public void sellHouse()
	{
		if(houses >0)
		{
			if (houses == 5)
			rent = rent4House;
			if (houses == 4)
			rent = rent3House;
			if (houses == 3)
			rent = rent2House;
			if (houses == 2) 
			rent = rent1House;
			if (houses == 1)
			rent = rent0House;
			houses--;
			owner.setMoney(houseCost/2);
		}
	}
	//amount paid by the players when in someone's property
	public void affect(Player p)
	{
		if (!(owner==null) && !(p.equals(owner)))
		{
			if (isMonopoly() && houses == 0)
				p.pay(owner,rent*2);
			else
				p.pay(owner,rent);
		}
	}
	
	//checking if the property belogs to a player
	public boolean isMonopoly()
	{
		if(!(owner==null))
		{
			int count = 0;
			for(Property p : owner.getMyProps())
				if (p.getSetName().equalsIgnoreCase(setName))
					count ++;
			return (count == setNum);
		}
		else return false;
	}
	
	//check if all properties in the same set are evenly built
	public boolean canBuild() {
		boolean result = true;
		for(Property p : owner.getMyProps())
			if (p.getHouses() < houses) result = false;
		return result;
	}
	
	//check if all properties in the same set are evenly sold
	public boolean canSell() {
		boolean result = true;
		for(Property p : owner.getMyProps())
			if (p.getHouses() > houses) result = false;
		return result;
	}
	
	//setting the owner
	public void setOwner(Player p)
	{
		owner=p;
	}
	
	public Player getOwner()
	{
		return owner;	
	}
	public String getText()
	{
		return " pays " + rent + " to " + getOwner().getName(); 
	}

	@Override
	public String getSetName() {
		// TODO Auto-generated method stub
		return setName;
	}	
	public boolean hotelCheck() {
		boolean result = false;
		
		if(!(owner == null)) {
			result = true;
			for(Property p : owner.getMyProps()) 
				if(p.getSetName().equalsIgnoreCase(setName))
					if(p.getHouses()<4) result = false;
		}
		
		return result;
	}
	
	public void info() {
		System.out.println("-------------------------------");
		System.out.println("Name: "+ this.name);
		System.out.println("Set Name: " + this.setName);
		System.out.println("Owner: "+ this.owner.getName());
		System.out.println("Number of houses: " + this.houses);
		System.out.println("-------------------------------");
	}
}


