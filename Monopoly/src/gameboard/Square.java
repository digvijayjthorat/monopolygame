package gameboard;

public abstract class Square {
	private int pos; // 0~39, indicating the tile number on the gameboard.
	
	public Square (int pos){
		this.pos = pos;
	}
	
	/* (non-Javadoc)
	 * @see gameboard.Square#Action(player.Player)
	 */
	public abstract void Action(Player player) throws Exception;
	
	/* (non-Javadoc)
	 * @see gameboard.Square#getPos()
	 */
	public int getPos() {
		return pos;
	}

	public Property getProterty(int pos) {
		// TODO Auto-generated method stub
		return null;
	}

	

}
