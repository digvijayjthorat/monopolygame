package gameboard;

import java.util.List;

//Some functions for testing.
public class Backdoor {
	private boolean skipTurn;
	
	public Backdoor() {
		skipTurn = false;
	}
	public boolean check(String string) {
		return string.equalsIgnoreCase("test");
	}
	
	public void handle(Input input, Player player) {
	
		System.out.println("Backdoor enabled. What do you want to do?");
		System.out.println("1. Increase current player's money.");
		System.out.println("2. Add Get Out of Jail Card to current player.");
		System.out.println("3. Skip current turn.");
		System.out.println("4. Back to game.");
		
		switch(input.inputInt()) {
		case 1:{
			System.out.println("Current player is " + player.getName() + ".");
			System.out.println(player.getName() + " has " + player.getMoney() + " dollars");
			System.out.print("Increase the current player's money by...");
			player.setMoney(input.inputInt());
			handle(input,player);
			break;
		}
		case 2:{
			System.out.println("Current player is " + player.getName() + ".");
			System.out.print("Press Enter to add a Get Out of Jail card.");
			input.pressEnter();
			player.addJailCard();
			handle(input,player);
			break;
		}
		case 3:{
			System.out.println("Current player is " + player.getName() + ".");
			System.out.println("Press Enter to skip current turn.");
			input.pressEnter();
			skipTurn = true;
			break;
		}
		case 4: break;
		default :{
			System.out.println("Invalid input. Press the number accordingly.");
			handle(input,player);
			break;
		}
		}
	}
	
	public boolean skipTurn() {
		return skipTurn;
	}
}
