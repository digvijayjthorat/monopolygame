package gameboard;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class Tests {
	Player p= new Player("player");
	@Test 
	void SetLocation_DesiredLocation_FinalLocation() 
	{
	 int expected= 10;	
	 p.setLocation(expected);
	 int actual= p.getLocation();
	 assertEquals(expected, actual, "Location did not set to a desired position");
	}
	
	@Test
	void SetMoney_AddToInitialAmount_FinalAmount()
	{
     int expected= 1700;	
	 p.setMoney(200);
	 int actual= p.getMoney();
	 //The expected result should be 1700 as we're adding 200 to the initial amount which is 1500
	 assertEquals(expected, actual, "Money did not set to a desired value");
	}
	
	@Test
	void TotalPlayers_AddPlayer_FinalNumberOfPlayers()
	{
     int expected= 3;	
	 p.setTotalPlayer(3);
	 int actual= p.getTotalPlayers();
	 assertEquals(expected, actual, "Total players are not setting up");
	}

}
