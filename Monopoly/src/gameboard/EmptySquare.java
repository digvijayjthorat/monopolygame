package gameboard;

public class EmptySquare extends Square{
	private InputDeck chance= new Deck(true);
	private InputDeck community= new Deck(false);
    private Input input = new Input();
    private Property med,bal,ori,ver,con,cha,sta,vir,jam,ten,york,ken,ind,atl,ven,ill,mar,pac,car,pen,par,boa;
    Exception holdAuction;
    
	public EmptySquare(int pos) {
		super(pos);
		med= new Street("Mediterranean Avenue", "PURPLE", 2, 2, 10, 30, 90, 160, 250, 50, 60);
	    bal= new Street("Baltic Avenue","PURPLE",2,4,20,60,180,220,450,50,60);
	    ori = new Street("Oriental Avenue", "LIGHT_BLUE", 3, 6, 30, 90, 270, 400, 550, 50, 100);
		ver = new Street("Vermont Avenue", "LIGHT_BLUE", 3, 6, 30, 90, 270, 400, 550, 50, 100);
		con = new Street("Oriental Avenue", "LIGHT_BLUE", 3, 8, 40, 100, 300, 450, 600, 50, 100);
		cha = new Street("St. Charles Place", "PINK", 3, 10, 50, 150, 450, 625, 750, 100, 140);
		sta = new Street("States Avenue", "PINK", 3, 10, 50, 150, 450, 625, 750, 100, 140);
		vir = new Street("Virginia Avenue", "PINK", 3, 12, 60, 180, 500, 700, 900, 100, 160);
		jam = new Street("St. James Place", "ORANGE", 3, 14, 70, 200, 550, 750, 950, 100, 180);
		ten = new Street("Tennessee Avenue", "ORANGE", 3, 14, 70, 200, 550, 750, 950, 100, 180);
		york = new Street("New York Avenue", "ORANGE", 3, 16, 80, 220, 600, 800, 1000, 100, 200);
		ken = new Street("Kentucky Avenue", "RED", 3, 18, 90, 250, 700, 875, 1050, 150, 220);
		ind = new Street("Indiana Avenue", "RED", 3, 18, 90, 250, 700, 875, 1050, 150, 220);
		ill = new Street("Illinois Avenue", "RED", 3, 20, 100, 300, 750, 925, 1100, 150, 240);
		atl = new Street("Atlantic Avenue", "YELLOW", 3, 22, 110, 330, 800, 975, 1150, 150, 260);
		ven = new Street("Ventnor Avenue", "YELLOW", 3, 22, 110, 330, 800, 975, 1150, 150, 260);
		mar = new Street("Marvin Gardens", "YELLOW", 3, 24, 120, 360, 850, 1025, 1200, 150, 280);
		pac = new Street("Pacific Avenue", "GREEN", 3, 26, 130, 390, 900, 1100, 1275, 200, 300);
		car = new Street("North Carolina Avenue", "GREEN", 3, 26, 130, 390, 900, 1100, 1275, 200, 300);
		pen = new Street("Pennsylvania Avenue", "GREEN", 3, 28, 150, 450, 1000, 1200, 1400, 200, 320);
		par = new Street("Park Place", "BLUE", 2, 35, 175, 500, 1100, 1300, 1500, 200, 350);
		boa = new Street("Boardwalk", "BLUE", 2, 50, 200, 600, 1400, 1700, 2000, 200, 400);
	}
	
	public void Action(Player player) throws Exception {
		int pos= player.getLocation();
		switch(pos)
		{
		case 1:
			propertyAction(bal,player);
			break;
		case 3:
			propertyAction(med,player);
			break;
		case 6: 
			propertyAction(ori, player);
			break;
		case 8:
			propertyAction(ver, player);
			break;
		case 9: 
			propertyAction(con, player);
			break;
		case 11:
			propertyAction(cha, player);
			break;
		case 13:
	        propertyAction(sta, player);
	        break;
		case 14:
			propertyAction(vir, player);
			break;
		case 16:
			propertyAction(jam, player);
			break;
		case 18:
		    propertyAction(ten, player);
		    break;
		case 19: 
			propertyAction(york, player);
			break;
		case 21: 
			propertyAction(ken, player);
			break;
		case 23:
		    propertyAction(ind, player);
		    break;
		case 24:
	        propertyAction(ill, player);
	        break;
		case 26:
			propertyAction(atl, player);
			break;
		case 27: 
			propertyAction(ven, player);
			break;
		case 29:
			propertyAction(mar, player);
			break;
		case 31:
			propertyAction(pac, player);
			break;
		case 32:
			propertyAction(car, player);
			break;
		case 34:
			propertyAction(pen, player);
			break;
		case 37:
		    propertyAction(par, player);
		    break;
		case 39:
			propertyAction(boa, player);
			break;
		case 2: 
			System.out.println ("You landed on CommunityChest!  You draw:");
			community.shuffle();
			System.out.println (community.print());
			community.next(player);
			break;
		case 17:
			System.out.println ("You landed on CommunityChest!  You draw:");
			community.shuffle();
			System.out.println (community.print());
			community.next(player);
			break;
		case 33:
			System.out.println ("You landed on CommunityChest!  You draw:");
			community.shuffle();
			System.out.println (community.print());
			community.next(player);
			break;
		case 7: 
			System.out.println ("You landed on Chance!  You draw a chance card:");
			chance.shuffle();
			System.out.println (chance.print());
			chance.next(player);
			break;
		case 22:
			System.out.println ("You landed on Chance!  You draw a chance card:");
			chance.shuffle();
			System.out.println (chance.print());
			chance.next(player);
			break;
		case 36:
			System.out.println ("You landed on Chance!  You draw a chance card:");
			chance.shuffle();
			System.out.println (chance.print());
			chance.next(player);
			break;
		}

	}

	private void propertyAction(Property property, Player player) throws Exception {
		System.out.println ("You landed on "+property.getName());
		if(!(property.getOwner() == null)) {
			System.out.println("The property is already been purchased");
			if(((property.getOwner()).getName()).equalsIgnoreCase(player.getName())) System.out.println("Hello Owner.");
			else
			{
				System.out.println((property.getOwner()).getName() + " owns this property. You need to pay " + property.getRent() + " dollars.");
				player.pay(property.getRent());
				System.out.println(player.getName()+" now has "+player.getMoney() + " dollars. ");
				(property.getOwner()).setMoney(property.getRent());
			}
		}
		else
		{
			System.out.println("Would you like to purchase " +property.getName()+ " for " + property.getCost() + " dollars (Yes/No)?");
			if(input.inputBool())
			{
				player.pay(property.getCost());
				property.setOwner(player);
				player.get(property);
				System.out.println("--------------------------------------------------");
				System.out.printf("%-10s%40s%n", "Name : ", player.getName());
				System.out.printf("%-10s%40s%n", "Money: ", player.getMoney());
				System.out.printf("%-10s%40s%n", "Position: ", player.getLocation());
				System.out.println("Properties: ");	//	Iterable<Square> owned = player.properties();
				for(Property p : player.getMyProps())
					System.out.println(p.getName()+"("+p.getHouses()+")"+"("+p.getSetName()+")");
				System.out.println("--------------------------------------------------");
			}else throw holdAuction;
		}
	}
	
@Override
	public Property getProterty(int pos) {
		switch(pos)
		{
		case 1:
			return bal;
		case 3:
			return med;
		case 6: 
			return ori;
		case 8:
			return ver;
		case 9: 
			return con;
		case 11:
			return cha;
		case 13:
			return sta;
		case 14:
			return vir;
		case 16:
			return jam;
		case 18:
			return ten;
		case 19: 
			return york;
		case 21: 
			return ken;
		case 23:
			return ind;
		case 24:
			return ill;
		case 26:
			return atl;
		case 27: 
			return ven;
		case 29:
			return mar;
		case 31:
			return pac;
		case 32:
			return car;
		case 34:
			return pen;
		case 37:
			return par;
		case 39:
			return boa;
		default:
			return null;
	}
}
}


