package gameboard;

public class Go extends Square{

	public Go(int pos) {
		super(pos);
	}

	//The action for Go square is set in the game logic.
	public void Action(Player player) {
		System.out.println(player.getName() + " lands on Go square.");
	}

}
