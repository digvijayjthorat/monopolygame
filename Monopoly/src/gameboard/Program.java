package gameboard;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

class Program {
	private List<Square> squares;
	private List<Player> players;
	private Player player= new Player("");
	private Input input;
	private Backdoor backdoor;
	private InputDice dice;
	private int playerRemoved;
	private Player winner;
	private Program()
	{
		players = new LinkedList<>();
		input = new Input();
		playerRemoved = 0;
		System.out.println("Quick initialization? (Yes/No)");
		if (input.inputBool()) quickInitialization(players);
		else initializePlayers(input);
		initializeGameboard();
	}
	
	//Setup players. Configure their names and turn order.
	private void initializePlayers(Input input) {
		System.out.println("Initializing players...");
		System.out.println("How many total players?");
		
		int N = input.inputInt();
		player.setTotalPlayer(N);
		while (N < 2 || N > 6) {
			
			System.out.println("Must have between 2 and 6 players. Please try again.");
			if (N < 1)
				System.out.println("Must have at least one human player. Please try again.");
			if (N > 6)
				System.out.println("Cannot have more human players than total players. Please try again.");
			N = input.inputInt();
			
		}		
		
		int[] order = new int[N];
		for (int i = 0; i < N; i++) {
			System.out.println("Player " + (i + 1) + " name?");
            players.add(new Player(input.inputString()));
		}
		
		System.out.println("done.");
	}
	
	//Setup game board. Configure the type of squares and their position on the game board.
	private void initializeGameboard() {
		
		System.out.print("Initializing gameboard...");
		
		squares = new ArrayList<>(40);
		
		for(int i = 0; i < 40; i++)  {
			Square square = new EmptySquare (i);
			squares.add(i,square);
		}
		Square square0 = new Go(0);
		Square square4 = new IncomeTax(4);
		Square square10 = new Jail(10);
		Square square20 = new GotoJail(20);
		
		squares.add(0,square0);
		squares.add(4,square4);
		squares.add(10,square10);
		squares.add(20,square20);
		System.out.println("Done.");
	}
	
	//Decide what the player should do on this square, based on the player's location.
	public void handleSquare(Player player) { 
		
		Square block = squares.get(player.getLocation());
		try {
			block.Action(player);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	private void startTurn(int turn) {
		Player currentPlayer;
		//Decide the current player, based on the number of turn. It will cycle through all players.
		currentPlayer = players.get((turn - 1) % (players.size())); 
		System.out.println("Turn " + turn + ". It is " + currentPlayer.getName() + "'s turn.");
		printState(currentPlayer);
		System.out.println("Press Enter to continue.");
		
		dice = new Dices();
		backdoor = new Backdoor();
		if(backdoor.check(input.inputString())) backdoor.handle(input,currentPlayer);
		if(backdoor.skipTurn()) startTurn(turn + 1);
		else {
		// First check if the current player is in jail.
		if(currentPlayer.isInJail()) handleSquare(currentPlayer); 
		
		//If the current player is not in jail, the player can proceed the turn normally.
		if(!currentPlayer.isInJail()) {
			dice.roll();
			currentPlayer.setLocation(currentPlayer.getLocation() + dice.getResult()); //Move the token
			handleSquare(currentPlayer);
			
			//If the player roll double less than 3 times in a roll, the player can roll again.
			while (dice.isDouble() && dice.doubleTimes() < 3 && !currentPlayer.isInJail()) {
				dice.roll( );
				currentPlayer.setLocation(currentPlayer.getLocation() + dice.getResult()); //Move the token
				handleSquare(currentPlayer);
			}
			
			if (dice.doubleTimes() == 3) {
				System.out.println("Three doubles in a roll. " + currentPlayer.getName() + " is sent to jail. Press Enter to continue.");
				input.pressEnter();
				currentPlayer.goToJail();
			}
		}
		
		//Bankruptcy check.
		if(currentPlayer.getMoney()==0 && currentPlayer.checkValue()<=0) {
			System.out.println(currentPlayer.getName() + " is bankrupt. Remove from game.");
			players.remove(players.indexOf(currentPlayer));
			playerRemoved++;
		}
		
		//Endgame check.
		if(playerRemoved >=2) {
			winner = players.get(0);
			for(int i = 0; i<players.size();i++) 
				if ((players.get(i)).checkValue() > winner.checkValue()) winner = players.get(i);
			System.out.println("Game Over. Winner is " + winner.getName());
			main(null);
		}
		System.out.println("Press Enter to start next turn.");
		input.pressEnter();
		startTurn(turn + 1); // Start the next turn.
		
		}
	}
	
	private void printState(Player player) {
		    System.out.println("--------------------------------------------------");
			System.out.printf("%-10s%40s%n", "Name : ", player.getName());
			System.out.printf("%-10s%40s%n", "Money: ", player.getMoney());
			System.out.printf("%-10s%40s%n", "Position ", player.getLocation());
			System.out.printf("Properties: ");	//	Iterable<Square> owned = player.properties();
			for(Property p : player.getMyProps())
				System.out.println(p.getName()+"("+p.getHouses()+")");
			System.out.println("--------------------------------------------------");
		}
	
	//Initialize 6 players with naming Player1, Player2...
	private void quickInitialization(List<Player> players) {
		System.out.println("How many total players?");
		int N = input.inputInt();
		for (int i=1;i<=N;i++)
			players.add(new Player("Player" + i+""));
	}
	
public static void main(String[] args) {
		Program monopolyGame= new Program();
		monopolyGame.startTurn(1);
	}

}
