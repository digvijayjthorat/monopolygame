package gameboard;

import java.util.ArrayList;
import java.util.List;

public class Bank {
	int house,hotel;
    List<Player> players;
    Input input;
	
	Bank(){
		this.house = 32;
		this.hotel = 12;
		players = new ArrayList<>();
		input =new Input();
	}
	
	public void updatePlayers(List<Player> players) {
		this.players = players;
	}
	
	public void auction(Player player, Property property) {
		System.out.println("A auction is held to sell "+ property.getName() + ". ");
		
		List<Player> bidingPlayers = new ArrayList<>();
		copy(players,bidingPlayers);
		int N = bidingPlayers.indexOf(player);
		int index =N+1;
		Player currentBidingPlayer = player;
		int price = 0;
		
		while(!(bidingPlayers.size() == 1)) {
			if(index == bidingPlayers.size()) index = index%bidingPlayers.size();
			currentBidingPlayer = bidingPlayers.get(index);
			System.out.println(currentBidingPlayer.getName() + "'s turn. Do you want to bid on " + property.getName() + "? (yes/no)");
			
			if(!(input.inputBool())) {
				System.out.println(currentBidingPlayer.getName()+" folds.");
				bidingPlayers.remove(currentBidingPlayer);
				if(index == bidingPlayers.size()+1) index = bidingPlayers.size(); 
			}else {
				price = biding(currentBidingPlayer,price);
				index++;
			}
		}
		if(price!=0) {
			currentBidingPlayer = bidingPlayers.get(0);
			currentBidingPlayer.pay(price);
			property.setOwner(currentBidingPlayer);
			currentBidingPlayer.get(property);
			System.out.println(property.getName()+" is sold to "+currentBidingPlayer.getName()+" with "+price+" dollars.");
			property.info();
		}else {
			currentBidingPlayer = bidingPlayers.get(0);
			System.out.println(currentBidingPlayer.getName() + "'s turn. Do you want to bid on " + property.getName() + "? (yes/no)");
			if(!(input.inputBool())) {
				System.out.println(currentBidingPlayer.getName()+" folds.");
				System.out.println("Nobody wants "+ property.getName());
			}else {
				System.out.println("You are the only player who wants " + property.getName()+". Sold at printed price.");
				price = property.getCost();
				currentBidingPlayer.pay(price);
				property.setOwner(currentBidingPlayer);
				currentBidingPlayer.get(property);
				System.out.println(property.getName()+" is sold to "+currentBidingPlayer.getName()+" with "+price+" dollars.");
				property.info();
			}
		}
	}
	
	private int biding(Player player, int price) {
		System.out.println("How much do you want to bid?");
		int temp = input.inputInt();
		if(temp <= price) {
			System.out.println("Biding price should be higher than the current price.");
			return biding(player,price);
		}else {
			price = temp;
			System.out.println("You bid "+price+" dollars.");
			return price;
		}
		
	}
	
	private void copy(List src, List dest) {
		for(int i = 0; i < src.size(); i++) {
			Object obj = src.get(i);
			if(obj instanceof List) {
				dest.add(new ArrayList());
				copy((List)obj,(List)((List)dest).get(i));
			}else {
				dest.add(obj);
			}
		}
	}

}
